#include <DummyStream.h>

DummyStream::DummyStream()
{
}

DummyStream::~DummyStream()
{
}

int DummyStream::available()
{
    return 0;
}

int DummyStream::read()
{
    return -1;
}

int DummyStream::peek()
{
    return -1;
}

void DummyStream::flush()
{
}

size_t DummyStream::write(uint8_t)
{
    return 0;
}

size_t DummyStream::write(const uint8_t *buffer, size_t size)
{
    return 0;
}
