#ifndef MAIN_H
#define MAIN_H

#include <Arduino.h>

namespace Main {

enum {
    CHANNEL_NONE = -1,
    CHANNEL_HWSERIAL = 0,
    CHANNEL_BTSERIAL = 1,
    CHANNEL_TCPSERIAL = 2,
    NUM_CHANNELS
};

void init();
void loop();

Stream *channel(int num = -1);

void SecretFunction();
void SecretAction(int code);

void toggleBluetooth(int mode = -1);
void toggleWiFi(int mode = -1);

void motorProfile(int profile);


extern int g_CurrentChannel;
extern int g_Debug[NUM_CHANNELS];
extern char g_SerialBuffer[NUM_CHANNELS][255];
extern int g_SerialBufferPos[NUM_CHANNELS];

};

#endif
