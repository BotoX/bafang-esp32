#ifndef DUMMYSTREAM_H
#define DUMMYSTREAM_H

#include <Stream.h>

class DummyStream : public Stream
{
public:
    DummyStream();
    ~DummyStream();

    int available();
    int read();
    int peek();
    void flush();

    size_t write(uint8_t);
    size_t write(const uint8_t *buffer, size_t size);
};

#endif
