#ifndef BAFANG_H
#define BAFANG_H
#include <stdint.h>

namespace Bafang {

#define BAUD_RATE 1200
#define BYTE_TIME (1000/((BAUD_RATE)/(1+8+1)) + 1) // Start + 8 Data + Stop

extern HardwareSerial &DisplaySerial;
extern HardwareSerial &MotorSerial;
extern bool g_Proxy;

void OnDisplayRx(uint8_t c);
void OnMotorRx(uint8_t c);

void tick();

typedef void (*FCallback)(void *pUser, const uint8_t *pData, uint8_t dataLen);
bool QueueMotor(const uint8_t *pData, uint8_t dataLen, FCallback pfnCallback, void *pUser, uint16_t waitMs=0);
void PrintCallback(void *pUser, const uint8_t *pData, uint8_t dataLen);

void OnDisplayRequest(const sPacketDesc *packet, uint8_t *data, uint8_t dataLen);
void OnMotorResponse(const sPacketDesc *packet, uint8_t *data, uint8_t dataLen);

int TransformPASCode(uint8_t PasCode);

namespace DisplaySlave {

void OnData(uint8_t c);
void tick();

bool ReplyRawPacket(const sPacketDesc *packet, uint8_t *data, uint8_t dataLen);

}

namespace MotorMaster {

void OnData(uint8_t c);
void tick();

void SendRawPacket(const sPacketDesc *packet, uint8_t *data, uint8_t dataLen);

}

enum eMode {
    MODE_READ = 0x11,
    MODE_WRITE = 0x16,
};

enum eCommand {
    REQ_VOLTAGE = 0x01,
    REQ_ERROR = 0x08,
    REQ_CURRENT = 0x0A,
    REQ_BAT = 0x11,
    REQ_RPM = 0x20,
    REQ_MOVING = 0x31,
    REQ_UNK01 = 0x90,
    REQ_UNK02 = 0x21,
    REQ_UNK03 = 0x22,
    REQ_UNK04 = 0x24,
    REQ_UNK05 = 0x25,
    REQ_UNKMENU = 0x60,
    REQ_INFO = 0x51,
    REQ_BASIC = 0x52,
    REQ_PEDAL = 0x53,
    REQ_THROTTLE = 0x54,

    CMD_LIGHT = 0x1A,
    CMD_PAS_LEVEL = 0x0B,
    CMD_RPMLIMIT = 0x1F
};

struct sPacketDesc {
    eCommand cmd;
    struct sInfo {
        int len;
        bool crc;
    };
    sInfo req;
    sInfo res;
};

const sPacketDesc PacketDesc[] = {
    {REQ_VOLTAGE, {0, false}, {2, false}},
    {REQ_ERROR, {0, false}, {1, false}},
    {REQ_CURRENT, {0, false}, {1, true}},
    {REQ_BAT, {0, false}, {1, true}},
    {REQ_RPM, {0, false}, {3, false}},
    {REQ_MOVING, {0, false}, {1, true}},
    {REQ_UNK01, {0, false}, {2, true}},
    {REQ_UNK02, {0, true}, {1, false}},
    {REQ_UNK03, {0, true}, {1, false}},
    {REQ_UNK04, {0, true}, {1, false}},
    {REQ_UNK05, {0, true}, {1, false}},
    {REQ_UNKMENU, {0, true}, {0, false}},
    {CMD_LIGHT, {1, false}, {1, false}},
    {CMD_PAS_LEVEL, {1, true}, {0, false}},
    {CMD_RPMLIMIT, {2, true}, {0, false}}
};

enum {
    PAS_LEVEL_0 = 0,
    PAS_LEVEL_1 = 1,
    PAS_LEVEL_2 = 12,
    PAS_LEVEL_3 = 13,
    PAS_LEVEL_4 = 14,
    PAS_LEVEL_5 = 2,
    PAS_LEVEL_6 = 21,
    PAS_LEVEL_7 = 22,
    PAS_LEVEL_8 = 23,
    PAS_LEVEL_9 = 3,
    PAS_LEVEL_PUSH = 6
};

}
#endif
